package main

import (
	"database/sql"
	"log"

	"github.com/fanyang01/library/lib"
)

var conn *lib.Conn

const (
	dbname   = "library"
	user     = "lib_admin"
	password = "xxxxxxxx"
)

func init() {
	var db *sql.DB
	var err error
	db, err = sql.Open("postgres", "user="+user+" password="+password+" dbname="+dbname+" sslmode=disable")
	if err != nil {
		panic(err.Error())
	}
	conn, err = lib.NewConn(db)
	if err != nil {
		panic(err.Error())
	}
}

func main() {
	err := startServer()
	if err != nil {
		log.Fatal(err.Error())
	}
}
