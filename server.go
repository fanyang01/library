package main

import (
	"bufio"
	"database/sql"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/fanyang01/library/lib"
)

var (
	MIN_PRICE          float64 = 0
	MAX_PRICE          float64 = 10000
	MIN_YEAR           int64   = 1900
	MAX_YEAR           int64   = int64(time.Now().Year())
	LIMIT              int64   = 128
	CARD_TOKEN_EXPIRE          = time.Hour * 72
	ADMIN_TOKEN_EXPIRE         = time.Hour * 24

	cardPrivateKeyFile  = "tokenkey/app.rsa"
	cardPubKeyFile      = "tokenkey/app.rsa.pub"
	adminPrivateKeyFile = "tokenkey/Admin.rsa"
	adminPubKeyFile     = "tokenkey/Admin.rsa.pub"
	cardVerifyKey       []byte
	cardSignKey         []byte
	adminVerifyKey      []byte
	adminSignKey        []byte

	rootDir = "./frontend/"
	host    = "localhost"
	port    = "7070"
)

func init() {
	var err error
	cardSignKey, err = ioutil.ReadFile(cardPrivateKeyFile)
	if err != nil {
		log.Fatal(err)
	}
	cardVerifyKey, err = ioutil.ReadFile(cardPubKeyFile)
	if err != nil {
		log.Fatal(err)
	}
	adminSignKey, err = ioutil.ReadFile(adminPrivateKeyFile)
	if err != nil {
		log.Fatal(err)
	}
	adminVerifyKey, err = ioutil.ReadFile(adminPubKeyFile)
	if err != nil {
		log.Fatal(err)
	}
}

type postResponse struct {
	Success bool        `json:"success"`
	Data    interface{} `json:"data"`
	Error   string      `json:"error"`
}

func startServer() error {

	http.HandleFunc("/api/admin/", adminLoginHandler)
	http.HandleFunc("/api/book/file/", uploadHandler)
	http.HandleFunc("/api/book/", bookHandler)
	http.HandleFunc("/api/query/", queryBooksHandler)
	http.HandleFunc("/api/card/auth/", cardLoginHandler)
	http.HandleFunc("/api/card/", cardHandler)
	http.HandleFunc("/api/borrow/", borrowHandler)
	http.HandleFunc("/api/return/", returnHandler)

	http.HandleFunc("/query/", indexHandler)
	http.HandleFunc("/card/", indexHandler)
	http.HandleFunc("/books/", indexHandler)
	http.HandleFunc("/admin/", indexHandler)
	http.HandleFunc("/login/", indexHandler)
	http.Handle("/", http.FileServer(http.Dir(rootDir)))
	return http.ListenAndServe(host+":"+port, nil)
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, rootDir+"index.html")
}

func cardHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		cardQueryHandler(w, r)
	case "POST":
		checkAdminToken(cardAddHandler)(w, r)
	case "DELETE":
		checkAdminToken(cardDeleteHandler)(w, r)
	}
}

func bookHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		bookQueryHandler(w, r)
	case "POST":
		checkAdminToken(bookAddHandler)(w, r)
	}
}

func checkAdminToken(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("AdminAccessToken")
		if tokenString == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		token, err := jwt.Parse(tokenString, func(*jwt.Token) (interface{}, error) {
			return adminVerifyKey, nil
		})

		if token.Valid {
			handler(w, r)
			return
		}
		if err != nil {
			log.Println(err)
		}
		w.WriteHeader(http.StatusForbidden)
	}
}

func checkCardToken(w http.ResponseWriter, r *http.Request) (*jwt.Token, bool) {
	tokenString := r.Header.Get("CardAccessToken")
	if tokenString == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return nil, false
	}

	token, err := jwt.Parse(tokenString, func(*jwt.Token) (interface{}, error) {
		return cardVerifyKey, nil
	})
	if token.Valid {
		return token, true
	}
	if err != nil {
		log.Println(err)
	}
	w.WriteHeader(http.StatusForbidden)
	return nil, false
}

func bookQueryHandler(w http.ResponseWriter, r *http.Request) {
	bookNumber := r.FormValue("number")
	if bookNumber == "" {
		http.NotFound(w, r)
		return
	}
	encoder := json.NewEncoder(w)
	book, err := conn.QueryBookByNo(bookNumber)
	// Not found
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}
	err = encoder.Encode(book)
	if err != nil {
		log.Println(err)
	}
}

func bookAddHandler(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	encoder := json.NewEncoder(w)
	var res postResponse
	var book lib.Book
	err := decoder.Decode(&book)
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}
	n, err := conn.QueryBookCountByNo(book.No)
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}
	if n <= 0 {
		err = conn.InsertBook(book)
	} else {
		err = conn.UpdateBookTotal(book.No, book.Total)
	}
	if err != nil {
		log.Println(err)
		res.Success = false
		res.Error = "更新数据库失败"
	} else {
		res.Success = true
	}
	err = encoder.Encode(res)
	if err != nil {
		log.Println(err)
	}
}

func queryBooksHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Println(err)
		return
	}
	books, err := conn.QueryBooks(parseQuery(r))
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}
	encoder := json.NewEncoder(w)
	err = encoder.Encode(books)
	if err != nil {
		log.Println(err)
	}
}

func parseQuery(r *http.Request) (flag, limit int64, query *lib.BooksQuery) {
	query = new(lib.BooksQuery)
	var s string
	if s = filter(r.FormValue("title")); s != "" {
		flag |= lib.QueryBooksByTitleFlag
		query.Title = s
	}
	if s = filter(r.FormValue("author")); s != "" {
		query.Author = s
		flag |= lib.QueryBooksByAuthorFlag
	}
	if s = filter(r.FormValue("press")); s != "" {
		query.Press = s
		flag |= lib.QueryBooksByPressFlag
	}
	if s = filter(r.FormValue("category")); s != "" {
		query.Category = s
		flag |= lib.QueryBooksByCategoryFlag
	}
	if r.FormValue("priceEnable") == "true" {
		var ok bool
		query.PriceLow, query.PriceHigh, ok = priceFilter(
			r.FormValue("priceLow"),
			r.FormValue("priceHigh"))
		if !ok {
			goto NEXT1
		}
		flag |= lib.QueryBetweenPriceFlag
	}
NEXT1:
	if r.FormValue("yearEnable") == "true" {
		var ok bool
		query.YearLow, query.YearHigh, ok = yearFilter(
			r.FormValue("yearLow"),
			r.FormValue("yearHigh"))
		if !ok {
			goto NEXT2
		}
		flag |= lib.QueryBetweenYearFlag
	}
NEXT2:
	if r.FormValue("stock") == "true" {
		flag |= lib.QueryStockNot0Flag
	}
	limit = int64(LIMIT)
	return
}

func filter(s string) string {
	s = strings.TrimSpace(s)
	return s
}

func priceFilter(a, b string) (float64, float64, bool) {
	var f1, f2 float64
	var err error
	a = filter(a)
	b = filter(b)
	if a == "" {
		f1 = MIN_PRICE
	} else {
		f1, err = strconv.ParseFloat(a, 32)
		if err != nil {
			log.Println(err)
			return 0, 0, false
		}
	}
	if b == "" {
		f2 = MAX_PRICE
	} else {
		f2, err = strconv.ParseFloat(b, 32)
		if err != nil {
			log.Println(err)
			return 0, 0, false
		}
	}
	return f1, f2, true
}

func yearFilter(a, b string) (int, int, bool) {
	var i1, i2 int64
	var err error
	a = filter(a)
	b = filter(b)
	if a == "" {
		i1 = MIN_YEAR
	} else {
		i1, err = strconv.ParseInt(a, 10, 32)
		if err != nil {
			log.Println(err)
			return 0, 0, false
		}
	}
	if b == "" {
		i2 = MAX_YEAR
	} else {
		i2, err = strconv.ParseInt(b, 10, 32)
		if err != nil {
			log.Println(err)
			return 0, 0, false
		}
	}
	return int(i1), int(i2), true
}

func cardQueryHandler(w http.ResponseWriter, r *http.Request) {
	token, ok := checkCardToken(w, r)
	if !ok {
		return
	}
	encoder := json.NewEncoder(w)
	cardNo := r.FormValue("number")
	if cardNo == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if token.Claims["CardNumber"].(string) != cardNo {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	var cardBooks *lib.CardBooks
	cardBooks, err := conn.QueryCardWithBooks(cardNo)
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}
	err = encoder.Encode(cardBooks)
	if err != nil {
		log.Println(err)
	}
}

func cardAddHandler(w http.ResponseWriter, r *http.Request) {
	var res postResponse
	var card lib.Card
	decoder := json.NewDecoder(r.Body)
	encoder := json.NewEncoder(w)

	err := decoder.Decode(&card)
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}
	err = conn.InsertCard(card)
	if err != nil {
		log.Println(err)
		res.Success = false
		res.Error = "更新数据库失败"
	} else {
		res.Success = true
	}
	err = encoder.Encode(res)
	if err != nil {
		log.Println(err)
	}
}

func cardDeleteHandler(w http.ResponseWriter, r *http.Request) {
	var res postResponse
	encoder := json.NewEncoder(w)

	cardNo := r.FormValue("number")
	if cardNo == "" {
		http.NotFound(w, r)
		return
	}
	slice := []string{cardNo}
	err := conn.DeleteCards(slice)
	if err != nil {
		log.Println(err)
		res.Success = false
		res.Error = "更新数据库失败"
	} else {
		res.Success = true
	}
	err = encoder.Encode(res)
	if err != nil {
		log.Println(err)
	}
}

func borrowHandler(w http.ResponseWriter, r *http.Request) {
	token, ok := checkCardToken(w, r)
	if !ok {
		return
	}
	decoder := json.NewDecoder(r.Body)
	encoder := json.NewEncoder(w)
	var t struct {
		CardNumber  string   `json:"cardNumber"`
		BookNumbers []string `json:"bookNumbers"`
	}
	err := decoder.Decode(&t)
	if err != nil {
		log.Println(err)
		return
	}
	if t.CardNumber != token.Claims["CardNumber"] {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	var res postResponse
	var borrows []lib.Borrow
	now := time.Now()
	for _, bn := range t.BookNumbers {
		n, err := conn.QueryBorrowedBookCount(t.CardNumber, bn)
		if err != nil {
			log.Println(err)
			return
		}
		if n > 0 {
			res.Success = false
			res.Error = "借书失败，你借出的编号为 " + bn + " 的书尚未归还"
			goto NEXT
		}
		borrows = append(borrows, lib.Borrow{
			CardNo:     t.CardNumber,
			BookNo:     bn,
			BorrowTime: now,
		})
	}

	err = conn.BorrowBooks(borrows)
	if err != nil {
		log.Println(err)
		res.Success = false
		res.Error = "更新数据库失败"
	} else {
		res.Success = true
	}
NEXT:
	err = encoder.Encode(res)
	if err != nil {
		log.Println(err)
	}
}

func returnHandler(w http.ResponseWriter, r *http.Request) {
	token, ok := checkCardToken(w, r)
	if !ok {
		return
	}
	decoder := json.NewDecoder(r.Body)
	encoder := json.NewEncoder(w)
	var t struct {
		CardNumber  string   `json:"cardNumber"`
		BookNumbers []string `json:"bookNumbers"`
	}
	err := decoder.Decode(&t)
	if err != nil {
		log.Println(err)
		return
	}
	if t.CardNumber != token.Claims["CardNumber"] {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	var borrows []lib.Borrow
	var res postResponse
	now := time.Now()
	for _, bn := range t.BookNumbers {
		n, err := conn.QueryBorrowedBookCount(t.CardNumber, bn)
		if err != nil {
			log.Println(err)
			return
		}
		if n <= 0 {
			res.Success = false
			res.Error = "还书失败，你没有未归还的编号为 " + bn + " 的书"
			goto NEXT
		}
		borrows = append(borrows, lib.Borrow{
			CardNo:     t.CardNumber,
			BookNo:     bn,
			ReturnTime: now,
		})
	}

	err = conn.ReturnBooks(borrows)
	if err != nil {
		log.Println(err)
		res.Success = false
		res.Error = "更新数据库失败"
	} else {
		res.Success = true
	}
NEXT:
	err = encoder.Encode(res)
	if err != nil {
		log.Println(err)
	}
}

func cardLoginHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	var res postResponse
	var card struct {
		No       string `json:"number"`
		Password string `json:"password"`
	}
	decoder := json.NewDecoder(r.Body)
	encoder := json.NewEncoder(w)
	err := decoder.Decode(&card)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	c, err := conn.QueryCard(card.No)
	if err != nil {
		if err == sql.ErrNoRows {
			res.Error = "帐号不存在"
			res.Success = false
		} else {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	} else {
		if c.Password != card.Password {
			res.Success = false
			res.Error = "密码错误"
			goto NEXT
		}
		res.Success = true
		token := jwt.New(jwt.SigningMethodRS256)
		token.Claims["CardNumber"] = card.No
		token.Claims["exp"] = time.Now().Add(CARD_TOKEN_EXPIRE).Unix()
		tokenString, err := token.SignedString(cardSignKey)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set("CardAccessToken", tokenString)
	}
NEXT:
	err = encoder.Encode(res)
	if err != nil {
		log.Println(err)
	}
}

func adminLoginHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	decoder := json.NewDecoder(r.Body)
	encoder := json.NewEncoder(w)
	var res postResponse
	var admin struct {
		ID       string `json:"id"`
		Password string `json:"password"`
	}
	err := decoder.Decode(&admin)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	a, err := conn.QueryAdminByID(admin.ID)
	if err != nil {
		if err == sql.ErrNoRows {
			res.Error = "帐号不存在"
			res.Success = false
		} else {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	} else {
		if admin.Password != a.Password {
			res.Success = false
			res.Error = "密码错误"
			goto NEXT
		}
		res.Success = true
		token := jwt.New(jwt.SigningMethodRS256)
		token.Claims["ID"] = admin.ID
		token.Claims["exp"] = time.Now().Add(ADMIN_TOKEN_EXPIRE).Unix()
		tokenString, err := token.SignedString(adminSignKey)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set("AdminAccessToken", tokenString)
	}
NEXT:
	err = encoder.Encode(res)
	if err != nil {
		log.Println(err)
	}
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	var res postResponse
	var books []lib.Book
	var err error
	encoder := json.NewEncoder(w)
	scanner := bufio.NewScanner(r.Body)
	for scanner.Scan() {
		b, err := parseLine(scanner.Text())
		if err != nil {
			log.Println(err)
			res.Success = false
			res.Error = "解析文件失败"
			goto NEXT
		}
		books = append(books, *b)
	}
	if err = scanner.Err(); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	err = conn.InsertBooks(books)
	if err != nil {
		log.Println(err)
		res.Success = false
		res.Error = "数据库更新失败"
	} else {
		res.Success = true
	}
NEXT:
	err = encoder.Encode(res)
	if err != nil {
		log.Println(err)
	}
}

// Each line of file is of following format:
// ( 书号, 类别, 书名, 出版社, 年份, 作者, 价格, 数量 )
func parseLine(line string) (*lib.Book, error) {
	s := strings.TrimSpace(line)
	s = strings.TrimPrefix(s, "(")
	s = strings.TrimSuffix(s, ")")
	s = strings.TrimSpace(s)
	slice := strings.Split(s, ",")
	if len(slice) != 8 {
		return nil, errors.New("bad format: " + line)
	}
	b := new(lib.Book)
	b.No = strings.TrimSpace(slice[0])
	b.Category = strings.TrimSpace(slice[1])
	b.Title = strings.TrimSpace(slice[2])
	b.Press = strings.TrimSpace(slice[3])
	i, err := strconv.ParseInt(strings.TrimSpace(slice[4]), 10, 64)
	if err != nil {
		return nil, err
	}
	b.Year = int(i)
	b.Author = strings.TrimSpace(slice[5])
	b.Price, err = strconv.ParseFloat(strings.TrimSpace(slice[6]), 64)
	if err != nil {
		return nil, err
	}
	i, err = strconv.ParseInt(strings.TrimSpace(slice[7]), 10, 64)
	if err != nil {
		return nil, err
	}
	b.Total = int(i)
	return b, nil
}
