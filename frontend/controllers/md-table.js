angular.module('md.table', ['ngMaterial'])
.directive('mdTable', function () {
  return {
    restrict: 'E',
    scope: { 
      headers: '=', 
      content: '=', 
      sortable: '=', 
      filters: '=',
      selected: '=',
      disabled: '='
    },
    controller: function ($scope,$filter,$window) {
      var orderBy = $filter('orderBy');
      	$scope.handleSort = function (field) {
          if ($scope.sortable.indexOf(field) > -1) { return true; } else { return false; }
      };
      $scope.order = function(predicate, reverse) {
          $scope.content = orderBy($scope.content, predicate, reverse);
          $scope.predicate = predicate;
      };
      $scope.order($scope.sortable[0],false);
      $scope.toggle = function(c) {
        idx = $scope.disabled.indexOf(c);
        if(idx >= 0)
          return;
        idx = $scope.selected.indexOf(c);
        if(idx < 0) {
          $scope.selected.push(c);
        } else {
          $scope.selected.splice(idx, 1);
        }
      };
    },
    template: '<div id="md-table-template">\
      <table class="md-table" style="border-collapse: collapse">\
        <thead>\
          <tr class="md-table-headers-row">\
            <th ng-repeat="h in headers" class="md-table-header"><a href="" ng-if="handleSort(h.field)" ng-click="reverse=!reverse;order(h.field,reverse)">{{h.name}} <i ng-show="reverse &amp;&amp; h.field == predicate" class="fa fa-sort-desc"></i><i ng-show="!reverse &amp;&amp; h.field == predicate" class="fa fa-sort-asc"></i></a><span ng-if="!handleSort(h.field)">{{h.name}}</span></th>\
            <th class="md-table-header"></th>\
          </tr>\
        </thead>\
        <tbody>\
          <tr ng-repeat="c in content | filter:filters" class="md-table-content-row" ng-click="toggle(c)">\
            <td ng-repeat="h in headers" ng-class="customClass[h.field]" class="md-table-content">{{c[h.field]}}</td>\
            <td><md-checkbox ng-checked="selected.indexOf(c) >= 0" ng-disabled="disabled.indexOf(c) >= 0" aria-label=""></md-checkbox></td>\
          </tr>\
        </tbody>\
      </table>\
    </div>\
    <style>\
    	a {\
    	  text-decoration: none;\
    	}\
    	i {\
    	  vertical-align: middle;\
    	  font-size: 24px;\
    	}\
    	.main-fab {\
    	  position:absolute;\
    	  z-index:20;\
    	  font-size:30px;\
    	  top:100px;\
    	  left:24px;\
    	  transform:scale(.88,.88);\
    	}\
    	.md-breadcrumb {\
    	  padding-left:88px;  \
    	}\
    	.JCLRgrip:first-child {\
    	  left:105px!important;\
    	  display: none!important;\
    	}\
    	  width: 105px!important;\
    	}\
    	.md-table {\
    	  min-width: 100%;\
    	  border-collapse: collapse;\
    	  border-spacing: 0;\
    	}\
    	.md-table tbody tr:hover, .md-table tbody tr:focus {\
    	  cursor:pointer;\
    	  background-color:rgba(63,81,181,0.2);\
    	}\
    	.md-table-header {\
    	  border-bottom: 1px solid rgb(230,230,230);\
    	  color: rgb(130,130,130);\
    	  text-align: left;\
    	  font-size: 0.75em;\
    	  font-weight: 700;\
    	  padding: 16px 16px 16px 0;\
    	}\
    	.md-table-header a{\
    	  text-decoration: none;\
    	  color: inherit;\
    	}\
    	.md-table-content {\
    	  white-space: nowrap;\
    	  font-size: 0.9em;\
    	  padding: 16px 16px 16px 0;\
    	  height: 60px;\
    	}\
    	.md-table-td-more {\
    	  max-width:72px;\
    	  width:72px;\
    	  padding:16px;\
    	}\
    	.bold {\
    	  font-weight: 700;\
    	}\
    	.grey {\
    	  color: grey;\
    	}\
    </style>'
  }
})