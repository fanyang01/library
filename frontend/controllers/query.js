angular.module('app')
.value('queryAPI', '/api/query/')
// server should response an array of book,
// with attributes (number, title, author, press, year, price, total, stock)
.factory('queryBooks', function($q, $http, queryAPI){
	return function(params){
		var options = {
			method: 'GET',
			url: queryAPI,
			params: params,
		};
		return $http(options)
			.then(function(response){
				return response.data;
			}, function(response) {
				return $q.reject(response.status + " " + response.statusText);
			});
	};
})
.controller('QueryCtrl', function($scope,
	$mdSidenav,
	queryBooks,
	$location,
	$rootScope,
	cardAuthService,
	messageToast,
	$timeout){
	$scope.books = {};
	$scope.books.list = [];
	$scope.book = {};

	$scope.signedIn = $rootScope.cardAuthData.number ? true : false;
	$scope.signOut = function() {
		cardAuthService.clearCredentials();
		$scope.signedIn = $rootScope.cardAuthData.number ? true : false;
		messageToast("注销成功");
	}
	$scope.showForm = true;
	$scope.$watch('book.number', function(){
		if($scope.book.number) {
			$scope.showForm = false;
		} else {
			$scope.showForm = true;
		}
	});

	$scope.now = Date.now();

	$scope.sidebarOpen = false;
	$scope.submit = function(click) {
		if($scope.book.number) {
			$location.path('/books/' + $scope.book.number);
		} else {
			queryBooks($scope.book)
			.then(function(books) {
				if(click) {
					$mdSidenav('right').toggle();
				}
				angular.copy(books, $scope.books.list);
			}, function(err) {
				$scope.err = "请求失败，错误信息: "+ err;
				messageToast($scope.err);
			})
		}
	};
	$scope.$watch(function() {
		return $mdSidenav('right').isOpen();
	},
	function() {
		$scope.sidebarOpen = $mdSidenav('right').isOpen();
	})
})
.controller('ResultCtrl', function($scope,
	$mdSidenav,
	queryBooks,
	borrowBooks,
	$timeout,
	$filter,
	messageToast
	){
	$scope.books.something = 'hello, world!';
	$scope.headers = [
		{
			name: "编号",
			field: "number"
		},{
			name: "分类",
			field: "category"
		},{
			name: "书名",
			field: "title"
		},{
			name: "作者",
			field: "author"
		},{
			name: "出版社",
			field: "press"
		},{
			name: "年份",
			field: "year"
		},{
			name: "价格",
			field: "price"
		},{
			name: "总计",
			field: "total"
		},{
			name: "库存",
			field: "stock"
		}
	];
	$scope.sortable = ['number', 'category', 'title', 'press', 'year','author', 'price', 'stock', 'total'];
	$scope.selected = [];
	$scope.disabled = [];
	$scope.close = function() {
		$mdSidenav('right').close();
	};
	$scope.$watch('books.list', function() {
		$scope.disabled = $filter('filter')($scope.books.list, function(value, index){
			if(value.stock > 0) {
				return false;
			} else {
				return true;
			}
		});
	}, true);
	$scope.$watch('selected', function() {
		if($scope.selected.length > 0)
			$scope.borrowable = true;
		else
			$scope.borrowable = false;
	}, true);

	$scope.borrow = function() {
		var bookNumbers = [];
		for (var i = $scope.selected.length - 1; i >= 0; i--) {
			if($scope.selected[i].stock > 0) {
				bookNumbers.push($scope.selected[i].number);
			}
		};
		if(bookNumbers.length <= 0) {
			messageToast("请至少选择一本书");
			return;
		}
		borrowBooks(bookNumbers)
		.then(function(data) {
			if(data.success) {
				messageToast('借书成功');
				$scope.selected = [];
				$scope.submit(false);
			} else {
				messageToast(data.error);
			}
		}, function(err) {
			messageToast("请求失败，错误信息: " + err);
		})
	}
})
