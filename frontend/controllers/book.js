angular.module('app')
.value('bookAPI', '/api/book/')
.factory('queryBook', function($http, $q, bookAPI){
	return function (number){
		var options = {
		    method: 'GET',
		    url: bookAPI,
		    params: {number: number}
		};
		return $http(options)
			.then(function(response) {
				return response.data;
			}, function(response) {
				return $q.reject(response.status + " " + response.statusText);
			});
	};
})
.controller('BookCtrl', function($scope,
	$location,
	$routeParams,
	queryBook,
	borrowBooks,
	$rootScope,
	cardAuthService,
	messageToast){
	$scope.signedIn = $rootScope.cardAuthData.number ? true : false;
	$scope.signOut = function() {
		cardAuthService.clearCredentials();
		$scope.signedIn = $rootScope.cardAuthData.number ? true : false;
		messageToast("注销成功");
	}
	$scope.book = {};
	$scope.update = function() {
		queryBook($routeParams.number)
		.then(function(book) {
			$scope.book = book;
		}, function(err) {
			$scope.err = "请求失败，错误信息: "+ err;
			messageToast($scope.err);
			$location.url('/query/');
		})
	};
	$scope.update();
	$scope.submit = function() {
		borrowBooks([$scope.book.number])
		.then(function(data) {
			if(data.success) {
				messageToast('借书成功');
				$scope.update();
			} else {
				messageToast(data.error);
			}
		}, function(err) {
			messageToast("请求失败，错误信息: " + err);
		})
	}
})
