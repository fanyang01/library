angular.module('app')
.value('cardAPI', '/api/card/')
.factory('getCard', function($http, $q, cardAPI){
	return function(number){
		var options = {
		    method: 'GET',
		    url: cardAPI,
		    params: {number: number}
		};
		return $http(options)
			.then(function(response) {
				return response.data;
			}, function(response) {
				return $q.reject(response.status + " " + response.statusText);
			});
	};
})
.controller('CardCtrl', function($scope,
	cardAuthService,
	getCard,
	messageToast,
	$filter,
	returnBooks,
	$rootScope,
	$location){
	$scope.signedIn = $rootScope.cardAuthData.number ? true : false;
	$scope.signOut = function() {
		cardAuthService.clearCredentials();
		$scope.signedIn = $rootScope.cardAuthData.number ? true : false;
		$location.url('/login/');
		messageToast("注销成功");
	}
	$scope.cardBooks = {};
	function format(cardBooks) {
		switch(cardBooks.card.type){
			case 0:
				cardBooks.card.type = '学生';
				break;
			case 1:
				cardBooks.card.type = '老师';
				break;
			default:
				cardBooks.card.type = '其他';
				break;
		}
		if(cardBooks.books) {
			for (var i = cardBooks.books.length - 1; i >= 0; i--) {
				cardBooks.books[i].borrowTime = $filter('date')(cardBooks.books[i].borrowTime, 'yyyy-MM-dd');
			};
		}
		return cardBooks;
	}
	$scope.update = function () {
		cardAuthService.getCardNumber()
		.then(getCard)
		.then(function(cardBooks) {
			$scope.cardBooks = format(cardBooks);
		}, function(err) {
			$scope.err = "请求失败，错误信息: "+ err;
			messageToast($scope.err);
		})
	};
	$scope.update();
	$scope.headers = [
		{
			name: "编号",
			field: "number"
		},{
			name: "分类",
			field: "category"
		},{
			name: "书名",
			field: "title"
		},{
			name: "作者",
			field: "author"
		},{
			name: "出版社",
			field: "press"
		},{
			name: "年份",
			field: "year"
		},{
			name: "价格",
			field: "price"
		},{
			name: "总计",
			field: "total"
		},{
			name: "库存",
			field: "stock"
		},{
			name: "借出时间",
			field: "borrowTime"
		}
	];
	$scope.sortable = ['number', 'category', 'title', 'press', 'year','author', 'price', 'stock', 'total', 'borrowTime'];
	$scope.selected = [];
	$scope.disabled = [];
	$scope.submit = function() {
		var bookNumbers = [];
		for (var i = $scope.selected.length - 1; i >= 0; i--) {
			bookNumbers.push($scope.selected[i].number);
		};
		if(bookNumbers.length <= 0) {
			messageToast("请选择至少一本书");
			return;
		}
		returnBooks(bookNumbers)
		.then(function(data) {
			if(data.success) {
				messageToast('还书成功');
				$scope.selected = [];
				$scope.update();
			} else {
				messageToast(data.error);
			}
		}, function(err) {
			messageToast("请求失败，错误信息: " + err);
		})
	}
})
