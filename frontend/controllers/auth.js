angular.module('app')
.value('cardAuthAPI', '/api/card/auth/')
.value('adminAuthAPI', '/api/admin/')
.factory('cardAuthService', function($q, $http, 
	$cookieStore, 
	$rootScope, 
	$location,
	cardAuthAPI){
	var Token = '';
	return {
		login: function (number, password){
			return $http.post(cardAuthAPI, {
				number: number,
				password: password
			})
			.then(function(response) {
				Token = response.headers('CardAccessToken');
				return response.data;
			}, function(response) {
				return $q.reject(response.status + ' ' + response.statusText);
			});
		},
		setCredentials: function(number, token) {
			$rootScope.cardAuthData = {
				number: number,
				token: Token
			}
			$http.defaults.headers.common['CardAccessToken'] = Token;
			$cookieStore.put('cardAuthData', $rootScope.cardAuthData);
		},
		clearCredentials: function() {
			Token = '';
			$rootScope.cardAuthData = {};
			$cookieStore.remove('cardAuthData');
			$http.defaults.headers.common['CardAccessToken'] = '';
		},
		getCardNumber: function() {
			var defered = $q.defer();
			if($rootScope.cardAuthData.number) {
				defered.resolve($rootScope.cardAuthData.number);
			} else {
				$q.reject('Not Login');
				$location.url('/login/');
			}
			return defered.promise;
		}
	};
})
.run(['$rootScope', '$location', '$http', '$cookieStore', 
	function($rootScope, $location, $http, $cookieStore){
		$rootScope.cardAuthData = $cookieStore.get('cardAuthData') || {};
		if($rootScope.cardAuthData.number) {
			$http.defaults.headers.common['CardAccessToken'] = $rootScope.cardAuthData.token;
		}
}])
.factory('adminAuthService', function($q, $http, 
	$cookieStore, 
	$rootScope, 
	$location,
	adminAuthAPI){
	var Token = '';
	return {
		login: function (id, password){
			return $http.post(adminAuthAPI, {
				id: id,
				password: password
			})
			.then(function(response) {
				Token = response.headers('AdminAccessToken');
				return response.data;
			}, function(response) {
				return $q.reject(response.status + ' ' + response.statusText);
			});
		},
		setCredentials: function(id, token) {
			$rootScope.adminAuthData = {
				id: id,
				token: Token
			}
			$http.defaults.headers.common['AdminAccessToken'] = Token;
			$cookieStore.put('adminAuthData', $rootScope.adminAuthData);
		},
		clearCredentials: function() {
			Token = '';
			$rootScope.adminAuthData = {};
			$cookieStore.remove('adminAuthData');
			$http.defaults.headers.common['AdminAccessToken'] = '';
		},
		checkLogin: function() {
			var defered = $q.defer();
			if($rootScope.adminAuthData.id && $rootScope.adminAuthData.token) {
				defered.resolve($rootScope.adminAuthData.id);
			} else {
				$q.reject('Not Login');
				$location.url('/login/');
			}
			return defered.promise;
		}
	};
})
.run(['$rootScope', '$location', '$http', '$cookieStore', 
	function($rootScope, $location, $http, $cookieStore){
		$rootScope.adminAuthData = $cookieStore.get('adminAuthData') || {};
		if($rootScope.adminAuthData.id) {
			$http.defaults.headers.common['AdminAccessToken'] = $rootScope.adminAuthData.token;
		}
}])