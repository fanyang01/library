/**
* app Module
*
* Root module
*/
angular.module('app', ['ngMaterial', 'ngRoute', 'ngCookies', 'md.table'])
.config(function($locationProvider) {
	$locationProvider.html5Mode(true);
})
.config(function($routeProvider) {
	$routeProvider
		.when('/', {
			redirectTo: '/login/'
		})
		.when('/query/', {
			templateUrl: '/view/query.html',
			controller: 'QueryCtrl',
		})
		.when('/card/', {
			templateUrl: '/view/card.html',
			controller: 'CardCtrl',
		})
		.when('/books/:number', {
			templateUrl: '/view/book.html',
			controller: 'BookCtrl',
		})
		.when('/login/', {
			templateUrl: '/view/login.html',
			controller: 'LoginCtrl',
		})
		.when('/admin/', {
			templateUrl: '/view/admin.html',
			controller: 'AdminCtrl',
		})
})
.factory('messageToast', function($mdToast, $animate){
	return function (message) {
		var toast = $mdToast.simple()
		      .content(message)
		      .action('关闭')
		      .highlightAction(false)
		      .hideDelay(4500)
		      .position("top right");
		$mdToast.show(toast);
	};
})
