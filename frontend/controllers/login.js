angular.module('app')
.controller('LoginCtrl', function($scope) {
	$scope.message = "I'm root ctrl!";
})
.controller('cardLoginCtrl', function($scope,
	cardAuthService,
	messageToast,
	$location){
	$scope.login = function() {
		cardAuthService.clearCredentials();
		cardAuthService.login($scope.cardNumber, $scope.password)
		.then(function(data) {
				if(data.success) {
					cardAuthService.setCredentials($scope.cardNumber);
					messageToast("登录成功");
					$location.path('/card/');
				} else {
					$scope.password = '';
					messageToast(data.error);
				}
		}, function(err) {
			messageToast("请求失败，错误信息: " + err);
		});
	}
}).controller('adminLoginCtrl', function($scope,
	adminAuthService,
	messageToast,
	$location){
	$scope.login = function() {
		adminAuthService.clearCredentials();
		adminAuthService.login($scope.id, $scope.password)
		.then(function(data) {
				if(data.success) {
					adminAuthService.setCredentials($scope.id);
					messageToast("登录成功");
					$location.path('/admin/');
				} else {
					$scope.password = '';
					messageToast(data.error);
				}
		}, function(err) {
			messageToast("请求失败，错误信息: " + err);
		});
	}
})
