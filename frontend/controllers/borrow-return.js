angular.module('app')
.value('borrowAPI', '/api/borrow/')
.value('returnAPI', '/api/return/')
.factory('borrowBooks', function($http, $q, borrowAPI, cardAuthService){
	return function (bookNumbers) {
		var defered = $q.defer();
		cardAuthService.getCardNumber()
		.then(function(number) {
				defered.resolve(
					$http.post(borrowAPI, {
						cardNumber: number,
						bookNumbers: bookNumbers
					})
					.then(function(response) {
						return response.data;
					}, function(response) {
						return $q.reject(response.status + " " + response.statusText);
					}));
			}, function(err) {
				defered.reject(err);
			});
		return defered.promise;
	};
})
.factory('returnBooks', function($http, $q, returnAPI, cardAuthService){
	return function (bookNumbers){
		var defered = $q.defer();
		cardAuthService.getCardNumber()
		.then(function(number) {
				defered.resolve(
					$http.post(returnAPI, {
						cardNumber: number,
						bookNumbers: bookNumbers
					})
					.then(function(response) {
						return response.data;
					}, function(response) {
						return $q.reject(response.status + " " + response.statusText);
					}));
			}, function(err) {
				defered.reject(err);
			});
		return defered.promise;
	};
})
