angular.module('app')
.factory('addBook', function($http, $q, bookAPI){
    return function (book){
        return $http.post(bookAPI, book)
        .then(function(response) {
            return response.data;
        }, function(response) {
            return $q.reject(response.status + " " + response.statusText);
        })
    };
})
.value("uploadAPI", '/api/book/file/')
.factory('uploadFile', function($http, $q, uploadAPI){
    return function (content){
        return $http.post(uploadAPI, content)
        .then(function(response) {
            return response.data;
        }, function(response) {
            return $q.reject(response.status + " " + response.statusText);
        })
    };
})
.factory('addCard', function($http, $q, cardAPI){
    return function (card){
        return $http.post(cardAPI, card)
        .then(function(response) {
            return response.data;
        }, function(response) {
            return $q.reject(response.status + " " + response.statusText);
        })
    };
})
.factory('deleteCard', function($http, $q, cardAPI){
    return function (cardNumber){
        var options = {
            method: 'DELETE',
            url: cardAPI,
            params: {number: cardNumber}
        };
        return $http(options)
        .then(function(response) {
            return response.data;
        }, function(response) {
            return $q.reject(response.status + " " + response.statusText);
        })
    };
})
.directive("file", function () {
    return {
        scope: {
            file: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.file.metadata = changeEvent.target.files[0];
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.file.content = loadEvent.target.result;
                    });
                }
                reader.readAsText(scope.file.metadata);
            });
        }
    }
})
.controller('AdminCtrl', function($scope,
    messageToast,
    adminAuthService){
    adminAuthService.checkLogin()
    .then(function(id) {
        $scope.signedIn = true;
    });
    $scope.signOut = function() {
        adminAuthService.clearCredentials();
        messageToast("注销成功");
        adminAuthService.checkLogin();
    }
})
.controller('singleCtrl', function($scope,
    messageToast,
    $route,
    addBook){
    $scope.now = Date.now();
    $scope.submit = function() {
        addBook($scope.book)
        .then(function(data) {
            if(data.success) {
                messageToast("入库成功！");
                $route.reload();
            } else {
                messageToast(data.error);
            }
        }, function(err) {
            messageToast("请求失败，错误信息: " + err);
        });
    }
})
.controller('fileCtrl', function($scope,
    uploadFile,
    messageToast){
    $scope.file = {};
    $scope.getFile = function() {
        document.getElementById('upfile').click();
    };
    $scope.submit = function() {
        uploadFile($scope.file.content)
        .then(function(data) {
            if(data.success) {
                messageToast("入库成功！");
                $scope.book = {};
            } else {
                messageToast(data.error);
            }
        }, function(err) {
            messageToast("请求失败，错误信息: " + err);
        });
    }
})
.controller('addCardCtrl', function($scope,
    messageToast,
    $route,
    $element,
    addCard){
    function format(c) {
        var card = c;
        switch(card.type){
            case "0":
                card.type = 0;
                break;
            case "1":
                card.type = 1;
                break;
            default:
                card.type = 2;
                break;
        }
        return card;
    };
    $scope.submit = function() {
        var card = format($scope.card);
        addCard(card)
        .then(function(data) {
            if(data.success) {
                messageToast("添加成功！");
                $route.reload();
            } else {
                messageToast(data.error);
            }
        }, function(err) {
            messageToast("请求失败，错误信息: " + err);
        });
    }
})
.controller('deleteCardCtrl', function($scope,
    messageToast,
    deleteCard){
    $scope.submit = function() {
        deleteCard($scope.card.number)
        .then(function(data) {
            if(data.success) {
                messageToast("删除成功！");
                $scope.card.number = '';
            } else {
                messageToast(data.error);
            }
        }, function(err) {
            messageToast("请求失败，错误信息: " + err);
        });
    }
})
