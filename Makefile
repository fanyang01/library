all:
	go build main.go server.go

clean:
	psql -d library -U fan -f db/clean.sql
	# go clean

new:
	psql -d library -U fan -f db/db_config.sql
