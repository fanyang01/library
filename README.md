# Library Management System

This is an elementary library management system implemented with:

- [**PostgreSQL**](http://www.postgresql.org),
- [**Go**](https://golang.org),
- [**AngularJS**](https://angularjs.org/) and
- [**Angular Material Design**](https://material.angularjs.org).

## Build & Test

Debian/Ubuntu:

    # apt-get install postgresql postgresql-client
    # su - postgres
    $ psql
    postgres=# CREATE DATABASE library;
    postgres=# CREATE USER lib_admin WITH ENCRYPTED password 'xxxxxxxx';
    postgres=# GRANT CONNECT ON DATABASE library TO lib_admin;
    postgres=# \q
    $ exit
    # apt-get install golang git
    # [exit to your account]
    $ cd && mkdir go
    $ go get github.com/fanyang01/library
    $ su
    # su postgres
    $ psql -d library
    postgres=# \i [your-home-path]/go/src/github.com/fanyang01/library/db/db_config.sql
    postgres=# INSERT INTO admin (id, name, password) VALUES ("001", "foobar", "barfoo");
	postgres=# \q
    $ exit
    # exit
    $ cd go/src/github.com/fanyang01/library
    $ make
    $ ./main

Then open your browser and visit http://localhost:7070, enjoy!

## Demos

![login](demos/login.png)
![query0](demos/query0.png)
![query1](demos/query1.png)
![book](demos/book.png)
![card](demos/card.png)
![admin0](demos/admin0.png)
![admin1](demos/admin1.png)
