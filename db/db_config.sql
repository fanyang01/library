-- CREATE DATABASE library;

-- CREATE USER lib_admin WITH ENCRYPTED password 'xxxxxxxx';
-- GRANT CONNECT ON DATABASE library TO lib_admin;

DROP INDEX IF EXISTS index_card_cno;
DROP INDEX IF EXISTS index_book_year_price;
DROP INDEX IF EXISTS index_book_title;
DROP TABLE IF EXISTS borrow CASCADE;
DROP TABLE IF EXISTS book CASCADE;
DROP TABLE IF EXISTS card CASCADE;
DROP TABLE IF EXISTS admin CASCADE;

CREATE TABLE IF NOT EXISTS book (
	bno      varchar(13),
	title    varchar(128)   NOT NULL,
	author   varchar(64),
	press    varchar(64),
	year     int,           CHECK (year >= 1900),
	category varchar(16),
	price    numeric(5, 2), CHECK (price >= 0),
	total    int,           CHECK (total > 0),
	stock    int,           CHECK (stock >= 0),
	CHECK (stock <= total),
	PRIMARY KEY (bno)
);

CREATE TABLE IF NOT EXISTS card (
	cno        varchar(16),
	name       varchar(32) NOT NULL,
	password   varchar(64) NOT NULL,
	department varchar(16),
	type       int NOT NULL, CHECK (type IN (0, 1, 2)),
	PRIMARY KEY(cno)
);

CREATE TABLE IF NOT EXISTS admin (
	id       varchar(16),
	name     varchar(32) NOT NULL UNIQUE,
	password varchar(64) NOT NULL,
	phone    varchar(16),
	PRIMARY KEY (id)
);

-- status: 0 - borrowed, 1 - returned
CREATE TABLE IF NOT EXISTS borrow (
	cno         varchar(16),
	bno         varchar(13),
	borrow_time timestamp NOT NULL,
	return_time timestamp,
--	handle      numeric(10, 0),
	status      int NOT NULL, CHECK (status IN (0, 1)),
	FOREIGN KEY (cno) REFERENCES card
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (bno) REFERENCES book
		ON UPDATE CASCADE
		ON DELETE CASCADE
);

CREATE VIEW view_borrowed_book AS
	SELECT B.cno, B.borrow_time, book.*
	FROM borrow AS B, book
	WHERE book.bno = B.bno AND B.status = 0;

CREATE INDEX index_card_cno ON card(cno);
CREATE INDEX index_book_title ON book(title);
CREATE INDEX index_book_year_price ON book(year, price);

-- Set privileges
GRANT SELECT, INSERT, UPDATE ON book TO lib_admin;
GRANT SELECT, INSERT, UPDATE, DELETE ON card TO lib_admin;
GRANT SELECT, INSERT, UPDATE, DELETE ON borrow TO lib_admin;
GRANT SELECT ON admin TO lib_admin;
GRANT SELECT ON view_borrowed_book TO lib_admin;
