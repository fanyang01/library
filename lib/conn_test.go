package lib

import (
	"database/sql"
	"testing"
	"time"
)

var conn *Conn

const (
	dbname   = "library"
	user     = "lib_admin"
	password = "xxxxxxxx"
)

func init() {
	var db *sql.DB
	var err error
	db, err = sql.Open("postgres", "user="+user+" password="+password+" dbname="+dbname+" sslmode=disable")
	if err != nil {
		panic(err.Error())
	}
	conn, err = NewConn(db)
	if err != nil {
		panic(err.Error())
	}
}

func TestInsertQueryUpdate(t *testing.T) {
	timeConv := func(s string) time.Time {
		t, err := time.Parse("2006-Jan-02", s)
		if err != nil {
			panic(err.Error())
		}
		return t
	}
	books := []Book{
		{"10001000", "Database", "Database System  Concepts", "高等教育", 2013, "Abraham, Heney", 72.0, 2, 2},
		{"10001001", "Algorithm", "Introduction to Algorithms", "机械工业", 2010, "Thomas, Ronald", 128.0, 5, 5},
		{"10001002", "Language", "C++ Primer Plus", "人民邮电", 2014, "Stephen", 99.0, 5, 5},
		{"10001003", "Language", "C Primer Plus", "人民邮电", 2012, "Stephen", 60.0, 15, 15},
		{"10001004", "Compile", "Compliers: principles", "机械工业", 2009, "Alfred", 89.0, 5, 5},
		{"10001005", "Computer", "Computer System", "机械工业", 2013, "Someone", 54.0, 2, 2},
		{"10001006", "Algorithm", "Data Structures in C", "机械工业", 2010, "Mark Allen", 45.0, 9, 9},
		{"10001007", "Language", "The C Programming Language", "机械工业", 2001, "K&R", 35.0, 3, 3},
		{"10001008", "Network", "Computer Networks", "机械工业", 2008, "Douglas", 55.0, 4, 4},
		{"10001009", "Unix", "Advance Unix Programming", "人民邮电", 2011, "Richard", 128.0, 5, 5},
		{"10001010", "IR", "现代信息检索", "机械工业", 2010, "Another", 98.0, 10, 10},
	}
	err := conn.InsertBooks(books)
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	card := Card{
		"3130000", "XiaoSan", "password", "AB", 1,
	}
	err = conn.InsertCard(card)
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	borrows := []Borrow{
		{"3130000", "10001000", timeConv("2014-Feb-13"), timeConv("2014-Mar-01"), 0},
	}
	err = conn.BorrowBooks(borrows)
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	_, err = conn.QueryBooks(QueryBooksByTitleFlag, 100, &BooksQuery{
		Title: "Database",
	})
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	// fmt.Printf("%#v\n", *bs[0])
	_, err = conn.QueryCardWithBooks("3130000")
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	// fmt.Printf("%#v\n", *bbs[0])
	_, err = conn.Exec("DELETE FROM book")
	_, err = conn.Exec("DELETE FROM card")
}
