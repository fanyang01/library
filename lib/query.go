package lib

import (
	"errors"
	"fmt"

	_ "github.com/lib/pq"
) // import postgres driver

const (
	// query with following flags will return a slice of *Book
	// only one type of order permitted
	QueryBooksByTitleFlag = 1 << iota
	QueryBooksByCategoryFlag
	QueryBooksByAuthorFlag
	QueryBooksByPressFlag
	QueryBetweenPriceFlag
	QueryBetweenYearFlag
	QueryStockNot0Flag
	QueryHotBooksFlag
	// querys on table book
	bookSQL                  = ` bno, category, title, press, year, author, price, total, stock `
	queryBookByNoSQL         = `SELECT ` + bookSQL + ` FROM book WHERE bno = $1`
	queryBookCountByNoSQL    = `SELECT count(*) FROM book WHERE bno = $1`
	queryBookBaseSQL         = `SELECT ` + bookSQL + ` FROM book WHERE TRUE`
	queryBookByTitleSQL      = ` AND title ILIKE $%d`
	queryBookByAuthorSQL     = ` AND author ILIKE $%d`
	queryBookByCategorySQL   = ` AND category ILIKE $%d`
	queryBookByPressSQL      = ` AND press ILIKE $%d`
	queryBookStockNot0SQL    = ` AND stock > 0`
	queryLimitToSQL          = ` LIMIT $%d`
	queryBookBetweenPriceSQL = ` AND price >= $%d AND price <= $%d`
	queryBookBetweenYearSQL  = ` AND year >= $%d AND year <= $%d`
	orderByBorrowSQL         = ` ORDER BY total - stock DESC`
	// query on table card
	queryCardSQL = `SELECT cno, name, password, department, type FROM card WHERE cno = $1`
	// query on view view_borrowed_book
	queryBorrowedBooksSQL = `SELECT cno, borrow_time, ` + bookSQL +
		`  FROM view_borrowed_book WHERE cno = $1`
	queryBorrowedBookCountSQL = `SELECT count(*) FROM view_borrowed_book
		WHERE cno = $1 AND bno = $2`
	// query on table admin
	queryAdminByIDSQL = `SELECT id, name, password, phone FROM admin WHERE id = $1`
)

type BooksQuery struct {
	Title     string
	Category  string
	Author    string
	Press     string
	PriceLow  float64
	PriceHigh float64
	YearLow   int
	YearHigh  int
}

type BookBorrow struct {
	Borrow
	Book
}

type CardBooks struct {
	Card  `json:"card"`
	Books []*BookBorrow `json:"books"`
}

// QueryBookByNo returns book with given book number
func (conn *Conn) QueryBookByNo(no string) (*Book, error) {
	b := new(Book)
	err := conn.QueryRow(queryBookByNoSQL, no).Scan(
		&b.No,
		&b.Category,
		&b.Title,
		&b.Press,
		&b.Year,
		&b.Author,
		&b.Price,
		&b.Total,
		&b.Stock,
	)
	if err != nil {
		return nil, err
	}
	return b, nil
}

// QueryBookCountByNo tests if a book exists
func (conn *Conn) QueryBookCountByNo(bno string) (int, error) {
	stmt, err := conn.Prepare(queryBookCountByNoSQL)
	if err != nil {
		return -1, err
	}
	defer stmt.Close()
	var n int
	err = stmt.QueryRow(bno).Scan(&n)
	if err != nil {
		return -1, err
	}
	return n, nil
}

// QueryBooks takes a flag, which must bitwise-OR of flags defined as constants
func (conn *Conn) QueryBooks(flag, limit int64, q *BooksQuery) ([]*Book, error) {
	q.Title = fmt.Sprintf("%%%s%%", q.Title)
	q.Author = fmt.Sprintf("%%%s%%", q.Author)
	q.Category = fmt.Sprintf("%%%s%%", q.Category)
	q.Press = fmt.Sprintf("%%%s%%", q.Press)

	var query = queryBookBaseSQL
	var args []interface{}
	if flag&QueryBooksByTitleFlag != 0 {
		query += queryBookByTitleSQL
		args = append(args, q.Title)
	}
	if flag&QueryBooksByCategoryFlag != 0 {
		query += queryBookByCategorySQL
		args = append(args, q.Category)
	}
	if flag&QueryBooksByAuthorFlag != 0 {
		query += queryBookByAuthorSQL
		args = append(args, q.Author)
	}
	if flag&QueryBooksByPressFlag != 0 {
		query += queryBookByPressSQL
		args = append(args, q.Press)
	}
	if flag&QueryBetweenPriceFlag != 0 {
		query += queryBookBetweenPriceSQL
		args = append(args, q.PriceLow, q.PriceHigh)
	}
	if flag&QueryBetweenYearFlag != 0 {
		query += queryBookBetweenYearSQL
		args = append(args, q.YearLow, q.YearHigh)
	}
	if flag&QueryStockNot0Flag != 0 {
		query += queryBookStockNot0SQL
	}
	if flag&QueryHotBooksFlag != 0 {
		query += orderByBorrowSQL
	}
	query += queryLimitToSQL
	args = append(args, limit)

	if len(args) == 0 {
		return nil, errors.New("unexpected query flag")
	}
	n := len(args)
	var seq []interface{}
	for i := 1; i <= n; i++ {
		seq = append(seq, i)
	}
	sql := fmt.Sprintf(query, seq...)
	return conn.queryBooks(sql, args...)
}

func (conn *Conn) queryBooks(query string, args ...interface{}) ([]*Book, error) {
	var books []*Book
	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		b := new(Book)
		err := rows.Scan(
			&b.No,
			&b.Category,
			&b.Title,
			&b.Press,
			&b.Year,
			&b.Author,
			&b.Price,
			&b.Total,
			&b.Stock,
		)
		if err != nil {
			return nil, err
		}
		books = append(books, b)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return books[:len(books)], nil
}

// QueryCard query and return card whose card number is 'no'
func (conn *Conn) QueryCard(no string) (*Card, error) {
	stmt, err := conn.Prepare(queryCardSQL)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	c := new(Card)
	err = stmt.QueryRow(no).Scan(
		&c.No,
		&c.Name,
		&c.Password,
		&c.Department,
		&c.Type,
	)
	if err != nil {
		return nil, err
	}
	return c, nil
}

// QueryCardWithBooks query books borrowed with card number 'no'
func (conn *Conn) QueryCardWithBooks(no string) (*CardBooks, error) {
	stmtCard, err := conn.Prepare(queryCardSQL)
	if err != nil {
		return nil, err
	}
	defer stmtCard.Close()
	stmt, err := conn.Prepare(queryBorrowedBooksSQL)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	cb := new(CardBooks)

	err = stmtCard.QueryRow(no).Scan(
		&cb.Card.No,
		&cb.Card.Name,
		&cb.Card.Password,
		&cb.Department,
		&cb.Type,
	)
	if err != nil {
		return nil, err
	}
	// NOTE:
	// Clean password field in result
	cb.Card.Password = ""

	rows, err := stmt.Query(no)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		bb := new(BookBorrow)
		err := rows.Scan(
			&bb.CardNo,
			&bb.BorrowTime,
			&bb.BookNo,
			&bb.Category,
			&bb.Title,
			&bb.Press,
			&bb.Year,
			&bb.Author,
			&bb.Price,
			&bb.Total,
			&bb.Stock,
		)
		if err != nil {
			return nil, err
		}
		bb.No = bb.BookNo
		cb.Books = append(cb.Books, bb)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return cb, nil
}

// QueryBorrowedBookCount return number of book(bno) borrowed by card(cno)
func (conn *Conn) QueryBorrowedBookCount(cno, bno string) (int, error) {
	stmt, err := conn.Prepare(queryBorrowedBookCountSQL)
	if err != nil {
		return -1, err
	}
	defer stmt.Close()
	var n int
	err = stmt.QueryRow(cno, bno).Scan(&n)
	if err != nil {
		return -1, err
	}
	return n, nil
}

// QueryAdminByName returns admin that has name 'name'
func (conn *Conn) QueryAdminByID(ID string) (*Admin, error) {
	stmt, err := conn.Prepare(queryAdminByIDSQL)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	a := new(Admin)
	err = stmt.QueryRow(ID).Scan(
		&a.ID,
		&a.Name,
		&a.Password,
		&a.Phone,
	)
	if err != nil {
		return nil, err
	}
	return a, nil
}
