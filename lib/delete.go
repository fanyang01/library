package lib

import _ "github.com/lib/pq" // import postgres driver

const (
	deleteCardSQL = `DELETE FROM card WHERE cno = $1`
)

// DeleteCards delete cards with associated card numbers in nos
func (conn *Conn) DeleteCards(nos []string) error {
	tx, err := conn.Begin()
	if err != nil {
		return err
	}
	stmt, err := tx.Prepare(deleteCardSQL)
	if err != nil {
		return err
	}
	defer stmt.Close()
	for _, no := range nos {
		_, err := stmt.Exec(no)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}
