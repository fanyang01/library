package lib

import _ "github.com/lib/pq" // import postgres driver

const (
	borrowBookSQL = `INSERT INTO borrow (bno, cno, status, borrow_time) VALUES
					($1, $2, $3, $4)`
	decreaseBookStockSQL = `UPDATE book SET stock = stock - 1 WHERE bno = $1`
	returnBookSQL        = `UPDATE borrow SET status = 1, return_time = $1 WHERE cno = $2 AND bno = $3`
	increaseBookStockSQL = `UPDATE book SET stock = stock + 1 WHERE bno = $1`
	updateBooksTotalSQL  = `UPDATE book SET total = total + $1, stock = stock + $1 WHERE bno = $2`
)

// BorrowBooks inserts borrow records and update stock of books
func (conn *Conn) BorrowBooks(borrows []Borrow) (err error) {
	tx, err := conn.Begin()
	if err != nil {
		return err
	}
	stmt, err := tx.Prepare(borrowBookSQL)
	if err != nil {
		return err
	}
	defer stmt.Close()
	stmtUpdateStock, err := tx.Prepare(decreaseBookStockSQL)
	if err != nil {
		return err
	}
	defer stmtUpdateStock.Close()
	for _, br := range borrows {
		_, err := stmt.Exec(
			br.BookNo,
			br.CardNo,
			0, // 0 - borrowed
			br.BorrowTime,
		)
		if err != nil {
			tx.Rollback()
			return err
		}
		_, err = stmtUpdateStock.Exec(br.BookNo)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}

// ReturnBooks updates return_time and stock of books
func (conn *Conn) ReturnBooks(borrows []Borrow) error {
	tx, err := conn.Begin()
	if err != nil {
		return err
	}
	stmt, err := tx.Prepare(returnBookSQL)
	if err != nil {
		return err
	}
	defer stmt.Close()
	stmtUpdateStock, err := tx.Prepare(increaseBookStockSQL)
	if err != nil {
		return err
	}
	defer stmtUpdateStock.Close()
	for _, br := range borrows {
		_, err := stmt.Exec(br.ReturnTime, br.CardNo, br.BookNo)
		if err != nil {
			tx.Rollback()
			return err
		}
		_, err = stmtUpdateStock.Exec(br.BookNo)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}

// UpdateBookTotal update total and stock field of an existed book
func (conn *Conn) UpdateBookTotal(bno string, count int) error {
	stmt, err := conn.Prepare(updateBooksTotalSQL)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(count, bno)
	return err
}
