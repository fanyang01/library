package lib

import _ "github.com/lib/pq" // import postgres driver

const (
	insertBookSQL = `INSERT INTO book (bno, category, title, press, year, author, price, total, stock) VALUES
					($1, $2, $3, $4, $5, $6, $7, $8, $9)`
	insertCardSQL = `INSERT INTO card (cno, name, password, department, type) VALUES
					($1, $2, $3, $4, $5)`
	insertAdminSQL = `INSERT INTO admin (id, name, password, phone) VALUES
					($1, $2, $3, $4)`
)

// InsertBook insert a single book to library
func (conn *Conn) InsertBook(b Book) error {
	stmt, err := conn.Prepare(insertBookSQL)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(
		b.No,
		b.Category,
		b.Title,
		b.Press,
		b.Year,
		b.Author,
		b.Price,
		b.Total,
		b.Total,
		// b.Stock,
	)
	return err
}

// InsertBooks insert books in books to library on success, or none of them
func (conn *Conn) InsertBooks(books []Book) error {
	tx, err := conn.Begin()
	if err != nil {
		return err
	}
	stmt, err := tx.Prepare(insertBookSQL)
	if err != nil {
		return err
	}
	defer stmt.Close()
	for _, b := range books {
		_, err := stmt.Exec(
			b.No,
			b.Category,
			b.Title,
			b.Press,
			b.Year,
			b.Author,
			b.Price,
			b.Total,
			b.Total,
			// b.Stock,
		)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}

// InsertCard insert a single card
func (conn *Conn) InsertCard(c Card) error {
	stmt, err := conn.Prepare(insertCardSQL)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(
		c.No,
		c.Name,
		c.Password,
		c.Department,
		c.Type,
	)
	return err
}

// InsertCards insert all cards on success, or none of them
func (conn *Conn) InsertCards(cards []Card) error {
	tx, err := conn.Begin()
	if err != nil {
		return err
	}
	stmt, err := tx.Prepare(insertCardSQL)
	if err != nil {
		return err
	}
	defer stmt.Close()
	for _, c := range cards {
		_, err := stmt.Exec(
			c.No,
			c.Name,
			c.Password,
			c.Department,
			c.Type,
		)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}

// InsertAdmins insert administrators
func (conn *Conn) InsertAdmins(admins []Admin) (err error) {
	tx, err := conn.Begin()
	if err != nil {
		return err
	}
	stmt, err := tx.Prepare(insertAdminSQL)
	if err != nil {
		return err
	}
	defer stmt.Close()
	for _, a := range admins {
		_, err := stmt.Exec(
			a.ID,
			a.Name,
			a.Password,
			a.Phone,
		)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}
