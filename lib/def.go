package lib

import (
	"database/sql"
	"time"

	_ "github.com/lib/pq" // import postgres driver
)

// Conn is a connection to database
type Conn struct {
	*sql.DB
}

// Book contains all infomation about each book
type Book struct {
	No       string  `json:"number"`
	Category string  `json:"category"`
	Title    string  `json:"title"`
	Press    string  `json:"press"`
	Year     int     `json:"year"`
	Author   string  `json:"author"`
	Price    float64 `json:"price"`
	Total    int     `json:"total"`
	Stock    int     `json:"stock"`
}

// Borrow records the status of books
type Borrow struct {
	CardNo     string    `json:"cardNumber"`
	BookNo     string    `json:"bookNumber"`
	BorrowTime time.Time `json:"borrowTime"`
	ReturnTime time.Time `json:"returnTime"`
	// 0 - borrowed, 1 - returned
	Status int `json:"status"`
}

// Card is used to borrow books
type Card struct {
	No         string `json:"number"`
	Name       string `json:"name"`
	Password   string `json:"password"`
	Department string `json:"department"`
	// 0 - student, 1 - teacher, 2 - other
	Type int `json:"type"`
}

// Admin is the library administrator
type Admin struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Password string `json:"password"`
	Phone    string `json:"phone"`
}

// NewConn accept a db opened by sql.Open and return a connection
func NewConn(db *sql.DB) (conn *Conn, err error) {
	if err = db.Ping(); err != nil {
		return
	}
	conn = &Conn{
		DB: db,
	}
	return
}
